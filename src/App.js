import './App.css';
import ApplauseLottie from './dot-lotties/Multiple Applause-compressedj.lottie';
import SchoolBellLottie from './dot-lotties/school-bell-compressed.lottie'
function App() {
  return (
    <div className="App">
      <dotlottie-player
        src={ApplauseLottie}
        autoplay
        loop
        style={{ height: '300px', width: '300px' }}
      />
      <dotlottie-player
        src={SchoolBellLottie}
        autoplay
        loop
        style={{ height: '300px', width: '300px' }}
      />
    </div>
  );
}

export default App;
